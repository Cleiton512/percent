package ifsc.edu.br.appseekbar;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {
    SeekBar seekBar;
    TextView  textView;
    NumberFormat formatacaoPercentual = NumberFormat.getPercentInstance();
    private final SeekBar.OnSeekBarChangeListener seekBarChangeListener=  new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            textView.setText(formatacaoPercentual.format(seekBar.getProgress() / 100.0));
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        seekBar = findViewById(R.id.seekbar);
        textView = findViewById(R.id.text);
        seekBar.setOnSeekBarChangeListener(seekBarChangeListener);

    }


    public  void sincronizaTextView(){
        int valor= seekBar.getProgress();
        this.textView.setText(formatacaoPercentual.format(valor/100));
    }
}

